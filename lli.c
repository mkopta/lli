#include <stdio.h>
#include <stdlib.h>

struct ll {
	struct ll *next;
	int number;
};

unsigned list_size(struct ll *list) {
	unsigned size = 0;
	while (list) {
		size++;
		list = list->next;
	}
	return size;
}

void print_list(struct ll *list) {
	struct ll *it;
	it = list;
	while (it) {
		printf("%d ", it->number);
		it = it->next;
	}
	putchar('\n');
}

int list_eq(struct ll *a, struct ll *b) {
	if (list_size(a) != list_size(b))
		return 0;
	while (a) {
		if (a->number != b->number)
			return 0;
		a = a->next;
		b = b->next;
	}
	return 1;
}

struct ll *list_at(struct ll *list, unsigned at) {
	while (at--) {
		list = list->next;
		if (!list)
			return NULL;
	}
	return list;
}

int list_find(struct ll *list, int number) {
	int pos = 0;
	while (list) {
		if (list->number == number)
			return pos;
		list = list->next;
		pos++;
	}
	return -1;
}

struct ll *make_list_a(void) {
	int i;
	struct ll *a, *el, *it;
	a = malloc(sizeof(struct ll));
	it = a;
	it->number = 1;
	for (i = 0; i < 9; i++) {
		el = malloc(sizeof(struct ll));
		el->number = i + 2;
		it->next = el;
		it = el;
	}
	return a;
}

struct ll *make_list_b(void) {
	struct ll *it, *b = make_list_a();
	unsigned i, size = list_size(b);
	b = b->next;
	it = b;
	for (i = 0; i < size / 3; i++) {
		it->number += 1;
		it = it->next;
	}
	return b;
}

int find_mergepoint(struct ll *a, struct ll *b) {
	struct ll *ita = a, *itb = b;
	int mergepoint;
	while (ita) {
		while (itb) {
			if (ita->number == itb->number && list_eq(ita, itb))
				return mergepoint;
			itb = itb->next;
		}
		itb = b;
		ita = ita->next;
		mergepoint++;
	}
	return -1;
}

int main(void) {
	struct ll *a = make_list_a();
	struct ll *b = make_list_b();
	unsigned mergepoint_a, mergepoint_b;
	printf("List A: ");
	print_list(a);
	printf("List A size: ");
	printf("%u\n", list_size(a));
	printf("List B: ");
	print_list(b);
	printf("List B size: ");
	printf("%u\n", list_size(b));
	mergepoint_a = find_mergepoint(a, b);
	printf("Merge at %d of A\n", mergepoint_a);
	printf("A at %u: %d\n", mergepoint_a, list_at(a, mergepoint_a)->number);
	mergepoint_b = list_find(b, list_at(a, mergepoint_a)->number);
	printf("Merge at %d of B\n", mergepoint_b);
	printf("B at %u: %d\n", mergepoint_b, list_at(b, mergepoint_b)->number);
	return 0;
}
