all: clean run

run: lli
	./lli

lli: lli.c
	cc -Wall -Wextra -pedantic -ansi -o lli lli.c

clean:
	rm -f lli
